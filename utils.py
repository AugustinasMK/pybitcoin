import binascii

from bitcoin.rpc import RawProxy
import hashlib


def to_little_endian(hexstring):
    ba = bytearray.fromhex(hexstring)
    ba.reverse()
    s = ''.join(format(x, '02x') for x in ba)
    return s


class BFN:
    __p = RawProxy()

    def __get_transaction(self, tid):
        raw_tx = self.__p.getrawtransaction(tid)
        return self.__p.decoderawtransaction(raw_tx)

    def __get_index_output(self, tid, index):
        it = self.__get_transaction(tid)
        i = 0
        for output in it['vout']:
            if index == i:
                return output['value']
            i += 1

    def calculate_transaction_fee(self, tid):
        tx = self.__get_transaction(tid)
        vinput = 0
        voutput = 0

        for vin in tx['vin']:
            vinput += self.__get_index_output(vin['txid'], vin['vout'])

        for vout in tx['vout']:
            voutput += vout['value']
        return vinput - voutput

    def check_block_hash(self, height):
        blockhash = self.__p.getblockhash(int(height))
        header = self.__p.getblockheader(blockhash)

        header_hex = (to_little_endian(header['versionHex']) + to_little_endian(header['previousblockhash'])
                      + to_little_endian(header['merkleroot']) + to_little_endian(format(int(header['time']), 'x'))
                      + to_little_endian(header['bits']) + to_little_endian(format(int(header['nonce']), 'x')))

        header_bin = binascii.unhexlify(header_hex)
        correct_hash = to_little_endian(hashlib.sha256(hashlib.sha256(header_bin).digest()).hexdigest())

        if blockhash == correct_hash:
            print("The hashes match.")
        else:
            print('The hashes do not match!')
