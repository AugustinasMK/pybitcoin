# Checks if block hash matches the data.
from utils import BFN


class HashChecker:
    __B = BFN()
    def check_hash(self, height):
        self.__B.check_block_hash(height)