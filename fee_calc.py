from utils import BFN


class FeeCalculator:
    __B = BFN()
    def calculate(self, tid):
        print('The fee of transaction \'' + tid + '\' is ' + self.__B.calculate_transaction_fee(tid).__str__())
