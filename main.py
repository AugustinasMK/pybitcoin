import sys

from checker import HashChecker
from fee_calc import FeeCalculator

if len(sys.argv) == 1:
    print('Calculating fee of Alice\'s transaction')
    Calc = FeeCalculator()
    Calc.calculate("0627052b6f28912f2703066a912ea577f2ce4da4caa5a5fbd8a57286c345c2f2")
elif len(sys.argv) == 3:
    if sys.argv[1] == '1':
        print('Calculating fee of transaction \'' + sys.argv[2] + '\'')
        Calc = FeeCalculator()
        Calc.calculate(sys.argv[2])
    elif sys.argv[1] == '2':
        C = HashChecker()
        C.check_hash(sys.argv[2])
    else:
        print('Unvalid task type')
else:
    print("Unvalid parameters. Please provide the option of the program and the input id or leave the parameters blank for basic function.")
