# PyBitcoin
## Reikalavimai
1. Panaudojant `python-bitcoinlib` biblioteką, Python aplinkoje realizuokite programą, apskaičiuojančią  transakcijos (pvz.`0627052b6f28912f2703066a912ea577f2ce4da4caa5a5fbd8a57286c345c2f2`) mokestį?

   - Patobulinkite programą, kad ji gebėtų apskaičiuotų bet kurios įvestos Bitcoin transakcijos mokestį.
   - Apskaičiuokite, kiek kainavo atlikti (2019-09-06) vieną vertingiausių transakcijų Bitcoin tinkle, kurios hash'as yra: `4410c8d14ff9f87ceeed1d65cb58e7c7b2422b2d7529afc675208ce2ce09ed7d`

2. Panaudojant `python-bitcoinlib` biblioteką, Python aplinkoje realizuokite programą, kuri iš  atitinkamos bloko header'io informacijos "patikrintų", kad bloko hash'as yra teisingas.
## Naudojimo instrukcija
- Pasiruoškite python aplinką su `python-bitcoinlib` kompiuteryje su Bitcoin Full Node.
- Programa veikia dviem režimais:
-- Transakcijos mokesčio apskaičiavimas
-- Bloko hash'o sulyginimas su iš duomenų generuojamu hash'u.
- Programos paretmeruose nurodykite `1` ar `2` (programos rėžimas) ir `transakcijos` arba `bloko` id.

## Naudojamas Bitcoin Full Node
Naudojamas mano sukonfiguruotas `Bitcoin Full Node` paleistas ant virtualios Linux mašinos `GCP` platformoje.

Jei norite naudoti mano paleistą ir prižiūrimą Bitcoin Full node - susisiekite!
### Dabartiniai naudodojai
|Naudotojas|Kursas|
|----------|-----|
|Augustinas Makevičius|3|
|Jonas Šiukšteris|3|
|Rajmund Wysocki|3|
|Mindaugas Kasiulis|2|
|Žygimantas Augūnas|2|
|Ignas Dailydė|2|
|Juozas Rimantas|2|

## Changelog
### v1.1 - 2019-12-01
#### Pridėta
Įvykdyti reikalavimai
